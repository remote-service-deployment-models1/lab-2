import psycopg2
from tabulate import tabulate


# Подключение к PostgreSQL
def connection_to_db():
    connection = psycopg2.connect(
        user="postgres",
        password="admin",
        host="data_service",
        port="5432",
        database="postgres"
    )
    return connection


def get_students_and_birthdays():
    # Создание курсора и соединения
    connection = connection_to_db()
    cursor = connection.cursor()

    # Выполнение запроса и вывод данных
    cursor.execute("""
        SELECT student_name, student_surname, student_birth_date
        FROM student
        ORDER BY student_birth_date;
    """)

    # Получение результатов запроса
    students_data = cursor.fetchall()

    # Вывод в виде таблицы
    table_headers = ["Имя", "Фамилия", "Дата рождения"]
    print(tabulate(students_data, headers=table_headers, tablefmt="grid"))

    # Закрытие курсора и соединения
    cursor.close()
    connection.close()


if __name__ == "__main__":
    get_students_and_birthdays()


CREATE TABLE student (
student_id serial PRIMARY KEY,
student_name text NOT NULL,
student_surname text NOT NULL,
student_birth_date date NOT NULL
);

CREATE TABLE teacher (
teacher_id serial PRIMARY KEY,
teacher_name text NOT NULL,
teacher_surname text NOT NULL,
teacher_lastname text
);

CREATE TABLE subject (
subject_id serial PRIMARY KEY,
subject_date date,
subject_name text  NOT NULL,
subject_teacher_id integer REFERENCES teacher(teacher_id)
);

CREATE TABLE student_record (
record_id serial PRIMARY KEY,
student_id integer REFERENCES student(student_id),
subject_id integer REFERENCES subject(subject_id)
);


INSERT INTO student (student_name, student_surname, student_birth_date)
VALUES
('Адам', 'Иванов', '2002-07-01'),
('Давид', 'Петров', '2002-04-05'),
('Захар', 'Кузнецов', '2002-05-04'),
('Мир', 'Смирнов', '2002-12-06'),
('Рустам', 'Попов', '2002-11-10'),
('Уиллиам', 'Васильев', '2002-05-20'),
('Эмир', 'Соколов', '2002-07-25'),
('Яков', 'Волков', '2002-08-26'),
('Арина', 'Лебедева', '2002-09-24'),
('Сергей', 'Семенов', '2002-01-21'),
('Варвара', 'Егорова', '2002-11-23'),
('Ева', 'Степанова', '2002-10-10'),
('Иван', 'Орлова', '2002-04-15'),
('Элина', 'Никитина', '2002-03-14'),
('Марьяна', 'Ольгова', '2002-02-18');

INSERT INTO teacher (teacher_name, teacher_surname, teacher_lastname)
VALUES
('Арсений', 'Федосов', 'Александрович'),
('Валерий', 'Корольков', 'Артемович'),
('Виктор', 'Звягинцев', 'Максимович'),
('Владимир', 'Елизаров', 'Владимирович'),
('Лев', 'Грибов', ''),
('Иван', 'Калачев', 'Макарович');

INSERT INTO subject (subject_date, subject_name, subject_teacher_id)
VALUES
('2024-01-18', 'Математический анализ', 1),
('2024-01-10', 'Информатика', 3),
('2024-01-15', 'Философия', 6),
('2024-01-26', 'Английский язык', 5);


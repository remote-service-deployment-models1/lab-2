# lab-2
## Пояснение состава приложения
Данный репозиторий содержит две папки: 

1. <details><summary>data_service.</summary>В data_service будет находится скрипт init.sql, который должен будет создать в базе данных таблицу и заполнить её данными. 
</details>

2. <details><summary>application.</summary>В application находится приложение, написанное на Python, которое делает запрос к базе данных init.sql, а также Dockerfile для сборки контейнера.
</details>

Вне папок находятся файлы:

1. <details><summary>docker-compose.yaml.</summary> Файл docker-compose.yaml, который будет использоваться для развертывания обоих сервисов с помощью Docker Compose.  Скрипт init.sql будет примонтирован в директорию /docker-entrypoint-initdb.d/.
</details>

2. <details><summary>.gitlab-ci.yml.</summary>Файл .gitlab-ci.yml отвечает за настройку CI/CD системы. В нем же происходит запись отработанного питоновского кода в файл result2.txt.
</details>
